import pickle
import pandas as pd
import numpy as np

df = pickle.load(open("all_events.p", "rb"))

print(df.groupby('event').agg({'count'}))

#df.groupby('event').agg({'count'}).to_csv('counts.csv')

# cast timestamp to a timestamp
df['timestamp'] = pd.to_datetime(df['timestamp'])

# let's call the event 'Found match for scanned WiFi MAC address in bloom filter' the starting point
# first, identify all users                                                                                                                      first matching event
starting = df[df['event'] == 'Found match for scanned WiFi MAC address in bloom filter']

first_event_user = starting.groupby('user_id', as_index=False).agg({'timestamp': np.min})

# join all events to this df on user_id, then rank
merged = pd.merge(df, first_event_user, on='user_id')
# filter to events with timestamp later than first timestamp (as pandas can only do equi-joins)
later = merged.loc[(merged['timestamp_x'] > merged['timestamp_y'])]

# rank events by timestamp, partition by user_id
later['rank'] = later.sort_values(['timestamp_x'], ascending=[True]).groupby(['user_id']).cumcount() + 1
# filter to rank <= 3 events
filtered = later[later['rank'] <= 3]
# reshape table to be wide
wide = filtered.pivot(index='user_id',columns='rank',values='event')
wider = wide.reset_index() # index becomes column

# get the counts for all paths
paths = wider.groupby([1,2,3]).count()

paths=paths.rename(columns = {'user_id':'count'})

paths.to_csv('paths.csv')