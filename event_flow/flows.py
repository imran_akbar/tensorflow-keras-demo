"""5d16bb3794b5a9bb23cb6c4e70c97ee1
55d65f274f110c1598bbf760a56cd8f2

curl -u 5d16bb3794b5a9bb23cb6c4e70c97ee1:55d65f274f110c1598bbf760a56cd8f2 'https://amplitude.com/api/2/export?start=20151101T5&end=20151130T20' >> events.zip

gunzip *.gz
cat *.json > events #combine single file"""

import json
import pandas as pd
import pickle

out = open('parsed.json', 'w')
all_events = []

# create a pandas dataframe with just user id/event name/timestamp
with open('events.json', 'r') as f:
	for line in f.readlines():
		parsed = json.loads(line)
		data_to_keep = {'user_id': parsed.get('user_id'), 'event': parsed.get('event_type'), 'timestamp': parsed.get('event_time')}
		# 2015-11-29 08:15:58.131000
		# strip the timestamp of milliseconds
		timestamp = data_to_keep.get('timestamp')
		if timestamp:
			data_to_keep['timestamp'] = timestamp.split('.')[0]
		all_events.append(data_to_keep)

df = pd.DataFrame(all_events)
pickle.dump(df, open( "all_events.p", "wb" ))