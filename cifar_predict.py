from keras.models import load_model
import numpy as np
import scipy.misc

encoding = {
    0: 'airplane',
    1: 'automobile',
    2: 'bird',
    3: 'cat',
    4: 'deer',
    5: 'dog',
    6: 'frog',
    7: 'horse',
    8: 'ship',
    9: 'truck'
}

def load_and_scale_imgs():
    """
    load image & rescale to 32x32 pixels
    """
    img_names = ['airplane1.jpg']
 
    imgs = [scipy.misc.imresize(scipy.misc.imread(img_name), (32, 32))
           for img_name in img_names]
    return np.array(imgs, dtype='float32') / 255 # normalize levels between 0-1

model = load_model('image_recognition.h5')

imgs = load_and_scale_imgs()
predictions = model.predict_classes(imgs)
print([encoding.get(prediction) for prediction in predictions])