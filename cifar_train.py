from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.datasets import cifar10
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from keras.models import load_model
from keras.optimizers import SGD

import logging
import argparse
import sys
import os
import pickle

logger = logging.getLogger('keras')
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

class ImageClassifier():
    def __init__(self):
        """
        run all the operations in sequential order
        """
        self.load()
        self.pre_process()
        self.model()
        self.train()

    def load(self):
        """
        load in the images
        """
        try:
            self.batch_size = 32
            self.nb_classes = 100
            self.nb_epoch = 200

            # The data, shuffled and split between train and test sets:
            (self.X_train, self.y_train), (self.X_test, self.y_test) = cifar10.load_data()
            #logger.debug('X_train shape:', self.X_train.shape)
            #logger.debug(self.X_train.shape[0], 'train samples')
            #logger.debug(self.X_test.shape[0], 'test samples')

            # Convert class vectors to binary class matrices.
            self.Y_train = np_utils.to_categorical(self.y_train, self.nb_classes)
            self.Y_test = np_utils.to_categorical(self.y_test, self.nb_classes)

        except Exception as e:
            logger.error(e, exc_info=True)

    def pre_process(self):
        """
        generate augmented data (variants of the images) to increase training set
        """
        try:
            self.X_train = self.X_train.astype('float32')
            self.X_test = self.X_test.astype('float32')
            # converting the category labels to a one-hot encoding, then scaling the 8-bit RGB values to a 0-1.0 range
            self.X_train /= 255
            self.X_test /= 255

            logger.debug('Using data augmentation.')
            # This will do preprocessing and realtime data augmentation:
            self.datagen = ImageDataGenerator(
                featurewise_center=False,  # set input mean to 0 over the dataset
                samplewise_center=False,  # set each sample mean to 0
                featurewise_std_normalization=False,  # divide inputs by std of the dataset
                samplewise_std_normalization=False,  # divide each input by its std
                zca_whitening=False,  # apply ZCA whitening
                rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
                width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
                height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
                horizontal_flip=True,  # randomly flip images
                vertical_flip=False)  # randomly flip images

            # Compute quantities required for featurewise normalization
            # (std, mean, and principal components if ZCA whitening is applied).
            self.datagen.fit(self.X_train)

        except Exception as e:
            logger.error(e, exc_info=True)

    def model(self):
        """
        define the model as a stack of layers
        """
        try:
            model = Sequential()

            model.add(Convolution2D(32, 3, 3, border_mode='same',
                        input_shape=self.X_train.shape[1:]))
            model.add(Activation('relu'))
            model.add(Convolution2D(32, 3, 3))
            model.add(Activation('relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))
            model.add(Dropout(0.25))

            model.add(Convolution2D(64, 3, 3, border_mode='same'))
            model.add(Activation('relu'))
            model.add(Convolution2D(64, 3, 3))
            model.add(Activation('relu'))
            model.add(MaxPooling2D(pool_size=(2, 2)))
            model.add(Dropout(0.25))

            model.add(Flatten())
            model.add(Dense(512))
            model.add(Activation('relu'))
            model.add(Dropout(0.5))
            model.add(Dense(self.nb_classes))
            model.add(Activation('softmax'))

            # the first layer must have the same size as an input image (3, 32, 32) and the last dense layer must have the same number of outputs as number of classes we are using as labels (10). After the final dense layer is a softmax layer that squashes the output to a (0, 1) range that sums to 1.

            model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])
            self.model = model
        except Exception as e:
            logger.error(e, exc_info=True)

    def train(self):
        """
        run the model training - takes 2.5 hours on GPU
        """
        try:
            # Fit the model on the batches generated by datagen.flow().
            self.model.fit_generator(self.datagen.flow(self.X_train, self.Y_train,
                                             batch_size=self.batch_size),
                                samples_per_epoch=self.X_train.shape[0],
                                nb_epoch=self.nb_epoch,
                                validation_data=(self.X_test, self.Y_test))
            logger.debug('completed fit')
            
            self.model.save('image_recognition_sgd_augment.h5')
        except Exception as e:
            logger.error(e, exc_info=True)

if __name__ == '__main__':
    """
    run the ML model
    """
    classifier = ImageClassifier()